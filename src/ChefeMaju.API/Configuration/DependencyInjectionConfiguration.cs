﻿using ChefMaju.Infra.Ioc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ChefMaju.API.Configuration
{
    public static class DependencyInjectionConfiguration
    {
        public static void AddDIConfiguration(this IServiceCollection services, IConfiguration configuration)
        {
            Injector.RegisterContext(services);
            Injector.RegisterServices(services);
            Injector.RegisterRepository(services);
            Injector.RegisterOptions(services, configuration);
        }
    }
}
