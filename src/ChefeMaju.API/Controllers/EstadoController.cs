﻿using ChefMaju.Application.Interfaces;
using ChefMaju.Application.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChefMaju.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EstadoController : ControllerBase
    {
        private IEstadoAppService _estadoAppService;

        public EstadoController(IEstadoAppService estadoAppService)
        {
            _estadoAppService = estadoAppService;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IEnumerable<EstadoViewModel>> Get()
        {
            return await _estadoAppService.ObterEstados();
        }
    }
}