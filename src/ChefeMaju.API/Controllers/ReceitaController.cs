﻿using ChefMaju.Application.Interfaces;
using ChefMaju.Application.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ChefeMaju.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReceitaController : ControllerBase
    {
        IReceitaAppService _receitaAppService;

        public ReceitaController(IReceitaAppService receitaAppService)
        {
            _receitaAppService = receitaAppService;
        }

        [HttpGet]
        [Route("obter-por-cidade")]
        [AllowAnonymous]
        public async Task<ConsultaReceitaCidadeViewModel> ObterPorCidade([FromQuery]string cidade)
        {
            return await _receitaAppService.ObterReceitasPorCidade(cidade);
        }
    }
}