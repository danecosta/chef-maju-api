﻿using ChefMaju.Application.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChefeMaju.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CidadeController : ControllerBase
    {
        private ICidadeAppService _cidadeAppService;

        public CidadeController(ICidadeAppService cidadeAppService)
        {
            _cidadeAppService = cidadeAppService;
        }

        [HttpGet]
        [Route("obter-por-uf")]
        [AllowAnonymous]
        public async Task<IEnumerable<string>> ObterPorUf([FromQuery]string uf)
        {
            return await _cidadeAppService.ObterNomeCidadesPorUf(uf);
        }
    }
}