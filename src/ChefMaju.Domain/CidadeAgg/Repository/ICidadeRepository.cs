﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChefMaju.Domain.CidadeAgg.Repository
{
    public interface ICidadeRepository
    {
        Task<IEnumerable<Cidade>> ObterCidadesPorUf(string uf);
    }
}
