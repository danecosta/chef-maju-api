﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ChefMaju.Domain.CidadeAgg
{
    [DataContract]
    public class Cidade
    {
        [DataMember(Name = "nome")]
        public string Nome { get; set; }
    }
}
