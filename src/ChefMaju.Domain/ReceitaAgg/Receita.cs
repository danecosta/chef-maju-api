﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ChefMaju.Domain.ReceitaAgg
{
    [DataContract]
    public class Receita
    {
        [BsonId]
        [BsonRepresentation(BsonType.String)]
        public Guid Id { get; private set; }

        [DataMember(Name = "url")]
        public string LinkExterno { get; set; }

        [DataMember(Name = "label")]
        public string Nome { get; set; }

        [DataMember(Name = "image")]
        public string LinkImagem { get; set; }

        [DataMember(Name = "ingredientLines")]
        public IEnumerable<string> Ingredientes { get; set; }

        [DataMember(Name = "totalTime")]
        public string TempoPreparo { get; set; }

        public string Filtro { get; set; }
        public DateTime DataCadastro { get; set; }
    }
}
