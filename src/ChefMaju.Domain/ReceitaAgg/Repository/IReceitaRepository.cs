﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ChefMaju.Domain.ReceitaAgg.Repository
{
    public interface IReceitaRepository
    {
        public void SalvarReceitas(IEnumerable<Receita> receitas);
        public IEnumerable<Receita> ObterReceitas(string filtro);
        Task<IEnumerable<Receita>> ObterReceitasEdamam(string filtro);
    }
}
