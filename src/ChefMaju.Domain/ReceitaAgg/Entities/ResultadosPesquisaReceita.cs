﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ChefMaju.Domain.ReceitaAgg.Entities
{
    [DataContract]
    public class ResultadosPesquisaReceita
    {
        [DataMember(Name = "recipe")]
        public Receita Receita { get; set; }
    }
}
