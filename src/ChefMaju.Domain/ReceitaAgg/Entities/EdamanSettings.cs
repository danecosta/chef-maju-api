﻿namespace ChefMaju.Domain.ReceitaAgg.Entities
{
    public class EdamanSettings
    {
        public string Url { get; set; }
        public string AppId { get; set; }
        public string AppKey { get; set; }
        public int LimitMinute { get; set; }
        public int LimitMouth { get; set; }
    }
}
