﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ChefMaju.Domain.ReceitaAgg.Entities
{
    [DataContract]
    public class PesquisaReceita
    {
        [DataMember(Name = "hits")]
        public IEnumerable<ResultadosPesquisaReceita> Resultados { get; set; }
    }
}
