﻿using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace ChefMaju.Domain.EstadoAgg
{
    [DataContract]
    public class Estado
    {
        [DataMember(Name = "sigla")]
        public string Sigla { get; set; }

        [DataMember(Name = "nome")]
        public string Nome { get; set; }
    }
}
