﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChefMaju.Domain.EstadoAgg.Repository
{
    public interface IEstadoRepository
    {
        Task<IEnumerable<Estado>> ObterEstados();
    }
}
