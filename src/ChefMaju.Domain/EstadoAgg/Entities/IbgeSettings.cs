﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChefMaju.Domain.EstadoAgg.Entities
{
    public class IbgeSettings
    {
        public string UrlEstados { get; set; }
        public string UrlCidades { get; set; }
    }
}
