﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChefMaju.Domain.ClimaAgg.Repository
{
    public interface IClimaRepository
    {
        Task<Clima> ObterClimaPorCidadeOpenWeather(string cidade);
        Clima ObterClimaPorCidade(string cidade);
    }
}
