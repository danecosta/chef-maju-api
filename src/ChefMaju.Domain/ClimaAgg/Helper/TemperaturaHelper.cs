﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChefMaju.Domain.ClimaAgg.Helper
{
    public static class TemperaturaHelper
    {
        public static double KelvinToCelsius(double tempKelvin)
        {
            return tempKelvin - 273.15;
        }

        public static double CelciusToKelvin(double tempCelsius)
        {
            return tempCelsius + 273.15;
        }
    }
}
