﻿using ChefMaju.Domain.ClimaAgg.Entities;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ChefMaju.Domain.ClimaAgg
{
    [DataContract]
    public class Clima
    {
        [BsonId]
        [BsonRepresentation(BsonType.String)]
        public Guid Id { get; private set; }

        [DataMember(Name = "weather")]
        public IEnumerable<Tempo> Tempo { get; set; }

        [DataMember(Name = "main")]
        public Temperatura Temperatura { get; set; }

        public string Cidade { get; set; }

        public DateTime DataCadastro { get; set; }
    }
}
