﻿using System.Runtime.Serialization;

namespace ChefMaju.Domain.ClimaAgg.Entities
{
    [DataContract]
    public class Temperatura
    {
        [DataMember(Name = "temp")]
        public double TemperaturaAtual { get; set; }

        [DataMember(Name = "feels_like")]
        public double SensacaoTermica { get; set; }

        [DataMember(Name = "temp_min")]
        public double TemperaturaMinima { get; set; }

        [DataMember(Name = "temp_max")]
        public double TemperaturaMaxima { get; set; }

        [DataMember(Name = "humidity")]
        public double Umidade { get; set; }
    }
}
