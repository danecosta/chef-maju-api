﻿namespace ChefMaju.Domain.ClimaAgg.Entities
{
    public class OpenWeatherSettings
    {
        public string Url { get; set; }
        public string Key { get; set; }
        public int LimitMinute { get; set; }
        public int LimitMouth { get; set; }
    }
}
