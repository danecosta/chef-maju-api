﻿using System.Runtime.Serialization;

namespace ChefMaju.Domain.ClimaAgg.Entities
{
    [DataContract]
    public class Tempo
    {
        [DataMember(Name = "main")]
        public string Nome { get; set; }

        [DataMember(Name = "description")]
        public string Descricao { get; set; }
    }
}
