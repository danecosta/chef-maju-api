﻿namespace ChefMaju.Domain.Shared.Controle
{
    public interface IControleConsultaRepository
    {
        void SalvarConsulta(string api);
        bool VerificarLimiteConsulta(string api);
    }
}
