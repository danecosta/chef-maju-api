﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace ChefMaju.Domain.Shared.Controle
{
    public class ControleConsulta
    {
        [BsonId]
        [BsonRepresentation(BsonType.String)]
        public Guid Id { get; private set; }
        public string API { get; set; }
        public DateTime DataConsulta { get; set; }
    }
}
