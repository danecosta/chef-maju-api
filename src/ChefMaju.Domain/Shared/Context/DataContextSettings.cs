﻿namespace ChefMaju.Domain.Shared.Context
{
    public class DataContextSettings
    {
        public string ConnectionStringMongo { get; set; }
    }
}
