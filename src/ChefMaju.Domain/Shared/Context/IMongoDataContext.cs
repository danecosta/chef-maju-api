﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ChefMaju.Domain.Shared.Context
{
    public interface IMongoDataContext
    {
        void Inserir<T>(T registro);
        void Inserir<T>(IEnumerable<T> registros);
        void Remover<T>(Expression<Func<T, bool>> filtro);
        IEnumerable<T> Buscar<T>(Expression<Func<T, bool>> filtro = null);
        IEnumerable<T> Buscar<T>(string filter);
    }
}
