﻿namespace ChefMaju.Domain.Shared.Enums
{
    public class ApiEnum
    {
        public static string Edamam = "Edamam";
        public static string OpenWeather = "OpenWeather";
    }
}
