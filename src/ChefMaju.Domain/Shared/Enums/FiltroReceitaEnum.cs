﻿namespace ChefMaju.Domain.Shared.Enums
{
    public class FiltroReceitaEnum
    {
        public static string MaiorIgual30 = "salad";
        public static string MaiorIgual20 = "meat";
        public static string Menor20 = "soup";
    }
}
