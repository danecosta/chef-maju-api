﻿using ChefMaju.Application.Interfaces;
using ChefMaju.Application.Services;
using ChefMaju.Domain.CidadeAgg.Repository;
using ChefMaju.Domain.ClimaAgg.Entities;
using ChefMaju.Domain.ClimaAgg.Repository;
using ChefMaju.Domain.EstadoAgg.Entities;
using ChefMaju.Domain.EstadoAgg.Repository;
using ChefMaju.Domain.ReceitaAgg.Entities;
using ChefMaju.Domain.ReceitaAgg.Repository;
using ChefMaju.Domain.Shared.Context;
using ChefMaju.Domain.Shared.Controle;
using ChefMaju.Infra.Data.Context;
using ChefMaju.Infra.Data.Repository;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace ChefMaju.Infra.Ioc
{
    public class Injector
    {
        public static void RegisterContext(IServiceCollection services)
        {
            services.AddSingleton<IMongoDataContext, MongoDataContext>();
        }

        public static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<IEstadoAppService, EstadoAppService>();
            services.AddScoped<ICidadeAppService, CidadeAppService>();
            services.AddScoped<IReceitaAppService, ReceitaAppService>();
        }

        public static void RegisterRepository(IServiceCollection services)
        {
            services.AddScoped<ICidadeRepository, CidadeRepository>();
            services.AddScoped<IClimaRepository, ClimaRepository>();
            services.AddScoped<IEstadoRepository, EstadoRepository>();
            services.AddScoped<IReceitaRepository, ReceitaRepository>();
            services.AddScoped<IControleConsultaRepository, ControleConsultaRepository>();
        }

        public static void RegisterOptions(IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<DataContextSettings>(options =>
            {
                options.ConnectionStringMongo = configuration.GetSection("ConnectionStrings:MongoDb").Value;
            });

            services.Configure<EdamanSettings>(options =>
            {
                options.Url = configuration.GetSection("API:Edamam:url").Value;
                options.AppId = configuration.GetSection("API:Edamam:appId").Value;
                options.AppKey = configuration.GetSection("API:Edamam:appKey").Value;
                options.LimitMinute = Convert.ToInt32(configuration.GetSection("API:Edamam:limit-minute").Value);
                options.LimitMouth = Convert.ToInt32(configuration.GetSection("API:Edamam:limit-month").Value);
            });

            services.Configure<OpenWeatherSettings>(options =>
            {
                options.Url = configuration.GetSection("API:OpenWeatherMaps:url").Value;
                options.Key = configuration.GetSection("API:OpenWeatherMaps:key").Value;
                options.LimitMinute = Convert.ToInt32(configuration.GetSection("API:OpenWeatherMaps:limit-minute").Value);
                options.LimitMouth = Convert.ToInt32(configuration.GetSection("API:OpenWeatherMaps:limit-month").Value);
            });

            services.Configure<IbgeSettings>(options =>
            {
                options.UrlEstados = configuration.GetSection("API:Ibge:url-ufs").Value;
                options.UrlCidades = configuration.GetSection("API:Ibge:url-cidades").Value;
            });
        }
    }
}
