﻿using ChefMaju.Domain.ReceitaAgg;
using ChefMaju.Domain.ReceitaAgg.Entities;
using ChefMaju.Domain.ReceitaAgg.Repository;
using ChefMaju.Domain.Shared.Context;
using ChefMaju.Domain.Shared.Controle;
using ChefMaju.Domain.Shared.Enums;
using ChefMaju.Infra.Http.Clients;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace ChefMaju.Infra.Data.Repository
{
    public class ReceitaRepository : IReceitaRepository
    {
        private readonly string _url;
        private readonly string _appKey;
        private readonly string _appId;
        private readonly MajuClient _client;
        private readonly IControleConsultaRepository _controleRepository;
        private readonly IMongoDataContext _mongoDataContext;

        public ReceitaRepository(IOptions<EdamanSettings> settings,
            MajuClient client,
            IControleConsultaRepository controleRepository,
            IMongoDataContext mongoDataContext)
        {
            _url = settings.Value.Url;
            _appKey = settings.Value.AppKey;
            _appId = settings.Value.AppId;
            _client = client;
            _controleRepository = controleRepository;
            _mongoDataContext = mongoDataContext;
        }

        public void SalvarReceitas(IEnumerable<Receita> receitas)
        {
            _mongoDataContext.Inserir(receitas);
        }

        public IEnumerable<Receita> ObterReceitas(string filtro)
        {
            var result = _mongoDataContext.Buscar<Receita>(x => x.Filtro.Equals(filtro));
            return result;
        }

        public async Task<IEnumerable<Receita>> ObterReceitasEdamam(string filtro)
        {
            var url = _url.Replace("{consulta}", filtro).Replace("{appId}", _appId).Replace("{appKey}", _appKey);
            var responseAPI = await _client.Get(url);
            var response = TratarResponseEdamam(responseAPI);

            AtualizarReceitasMongo(filtro, response);

            _controleRepository.SalvarConsulta(ApiEnum.Edamam);

            return response;
        }

        private void AtualizarReceitasMongo(string filtro, IEnumerable<Receita> response)
        {
            if (response.Any())
            {
                // Remove receitas encontradas pelo filtro anteriormente
                _mongoDataContext.Remover<Receita>(x => x.Filtro.Equals(filtro));

                // Cadastra novas receitas para o filtro
                foreach (var novaReceita in response)
                {
                    novaReceita.DataCadastro = DateTime.Now;
                    novaReceita.Filtro = filtro;
                }
                _mongoDataContext.Inserir(response);
            }
        }

        private IEnumerable<Receita> TratarResponseEdamam(HttpResponseMessage response)
        {
            var result = new List<Receita>();
            switch (response.StatusCode)
            {
                case HttpStatusCode.OK:
                    var dataOK = response.Content.ReadAsStringAsync().Result;
                    var pesquisaReceita = JsonConvert.DeserializeObject<PesquisaReceita>(dataOK);
                    result = pesquisaReceita.Resultados.Select(x => x.Receita).ToList();
                    break;
                default:
                    break;
            }

            return result;
        }
    }
}
