﻿using ChefMaju.Domain.ClimaAgg;
using ChefMaju.Domain.ClimaAgg.Entities;
using ChefMaju.Domain.ClimaAgg.Repository;
using ChefMaju.Domain.Shared.Context;
using ChefMaju.Domain.Shared.Controle;
using ChefMaju.Domain.Shared.Enums;
using ChefMaju.Infra.Http.Clients;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace ChefMaju.Infra.Data.Repository
{
    public class ClimaRepository : IClimaRepository
    {
        private readonly string _url;
        private readonly string _key;
        private readonly MajuClient _client;
        private readonly IControleConsultaRepository _controleRepository;
        private readonly IMongoDataContext _mongoDataContext;

        public ClimaRepository(IOptions<OpenWeatherSettings> settings,
            MajuClient client,
            IControleConsultaRepository controleRepository,
            IMongoDataContext mongoDataContext)
        {
            _url = settings.Value.Url;
            _key = settings.Value.Key;
            _client = client;
            _controleRepository = controleRepository;
            _mongoDataContext = mongoDataContext;
        }

        public void SalvarClima(Clima clima)
        {
            _mongoDataContext.Inserir(clima);
        }

        public Clima ObterClimaPorCidade(string cidade)
        {
            var result = _mongoDataContext.Buscar<Clima>(x => x.Cidade.Equals(cidade));
            return result.FirstOrDefault();
        }

        public async Task<Clima> ObterClimaPorCidadeOpenWeather(string cidade)
        {
            var url = _url.Replace("{cidade}", cidade).Replace("{key}", _key);
            var responseAPI = await _client.Get(url);
            var response = TratarResponseOpenWeather(responseAPI);

            AtualizarClimaCidadeMongo(cidade, response);

            _controleRepository.SalvarConsulta(ApiEnum.OpenWeather);

            return response;
        }

        private void AtualizarClimaCidadeMongo(string cidade, Clima response)
        {
            if (response != null)
            {
                // Remove clima salvo anterior para a cidade pesquisada
                _mongoDataContext.Remover<Clima>(x => x.Cidade.Equals(cidade));

                // Cadastra novo clima para a cidade
                response.DataCadastro = DateTime.Now;
                response.Cidade = cidade;
                SalvarClima(response);
            }
        }

        private Clima TratarResponseOpenWeather(HttpResponseMessage response)
        {
            var result = new Clima();
            switch (response.StatusCode)
            {
                case HttpStatusCode.OK:
                    var dataOK = response.Content.ReadAsStringAsync().Result;
                    result = JsonConvert.DeserializeObject<Clima>(dataOK);
                    break;
                case HttpStatusCode.NotFound:
                    result = null;
                    break;
                default:
                    break;
            }

            return result;
        }
    }
}
