﻿using ChefMaju.Domain.CidadeAgg;
using ChefMaju.Domain.CidadeAgg.Repository;
using ChefMaju.Domain.EstadoAgg.Entities;
using ChefMaju.Infra.Http.Clients;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace ChefMaju.Infra.Data.Repository
{
    public class CidadeRepository : ICidadeRepository
    {
        private readonly string _urlCidade;
        private readonly MajuClient _client;

        public CidadeRepository(IOptions<IbgeSettings> settings,
            MajuClient client)
        {
            _urlCidade = settings.Value.UrlCidades;
            _client = client;
        }

        public async Task<IEnumerable<Cidade>> ObterCidadesPorUf(string uf)
        {
            var url = _urlCidade.Replace("{uf}", uf);
            var response = await _client.Get(url);

            return TratarResponseIbge(response);
        }

        private IEnumerable<Cidade> TratarResponseIbge(HttpResponseMessage response)
        {
            var result = new List<Cidade>();
            switch (response.StatusCode)
            {
                case HttpStatusCode.OK:
                    var dataOK = response.Content.ReadAsStringAsync().Result;
                    result = JsonConvert.DeserializeObject<List<Cidade>>(dataOK);
                    break;
                default:
                    break;
            }

            return result;
        }
    }
}
