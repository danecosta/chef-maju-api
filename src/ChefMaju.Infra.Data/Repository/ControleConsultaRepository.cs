﻿using ChefMaju.Domain.ClimaAgg.Entities;
using ChefMaju.Domain.ReceitaAgg.Entities;
using ChefMaju.Domain.Shared.Context;
using ChefMaju.Domain.Shared.Controle;
using Microsoft.Extensions.Options;
using System;
using System.Linq;

namespace ChefMaju.Infra.Data.Repository
{
    public class ControleConsultaRepository : IControleConsultaRepository
    {
        private readonly int _limitMinuteEdaman;
        private readonly int _limitMouthEdaman;

        private readonly int _limitMinuteOpenWeather;
        private readonly int _limitMouthOpenWeather;

        private readonly IMongoDataContext _mongoDataContext;

        public ControleConsultaRepository(IOptions<EdamanSettings> settingsEdaman,
            IOptions<OpenWeatherSettings> settingsOpenWeather,
            IMongoDataContext mongoDataContext)
        {
            _limitMinuteEdaman = settingsEdaman.Value.LimitMinute;
            _limitMouthEdaman = settingsEdaman.Value.LimitMouth;

            _limitMinuteOpenWeather = settingsOpenWeather.Value.LimitMinute;
            _limitMouthOpenWeather = settingsOpenWeather.Value.LimitMouth;

            _mongoDataContext = mongoDataContext;
        }

        public void SalvarConsulta(string api)
        {
            var controle = new ControleConsulta()
            {
                API = api,
                DataConsulta = DateTime.Now
            };

            _mongoDataContext.Inserir(controle);
        }

        public bool VerificarLimiteConsulta(string api)
        {
            var dataAtual = DateTime.Now;
            var consultasAPI = _mongoDataContext.Buscar<ControleConsulta>(x => x.API.Equals(api));
            var qtdConsultasDoMes = consultasAPI.Count(x => x.DataConsulta.Year == dataAtual.Year && x.DataConsulta.Month == dataAtual.Month);
            var qtdConsultasPorMinuto = consultasAPI.Count(x => x.DataConsulta == dataAtual);

            if (api.Equals("Edamam"))
            {
                if (qtdConsultasPorMinuto >= _limitMinuteEdaman) return false;

                return qtdConsultasDoMes < _limitMouthEdaman;
            }
            else if (api.Equals("OpenWeather"))
            {
                if (qtdConsultasPorMinuto >= _limitMinuteOpenWeather) return false;

                return qtdConsultasDoMes < _limitMouthOpenWeather;
            }
            return true;
        }
    }
}
