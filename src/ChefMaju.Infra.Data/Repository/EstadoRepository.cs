﻿using ChefMaju.Domain.EstadoAgg;
using ChefMaju.Domain.EstadoAgg.Entities;
using ChefMaju.Domain.EstadoAgg.Repository;
using ChefMaju.Infra.Http.Clients;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace ChefMaju.Infra.Data.Repository
{
    public class EstadoRepository : IEstadoRepository
    {
        private readonly string _urlUf;
        private readonly MajuClient _client;

        public EstadoRepository(IOptions<IbgeSettings> settings,
            MajuClient client)
        {
            _urlUf = settings.Value.UrlEstados;
            _client = client;
        }

        public async Task<IEnumerable<Estado>> ObterEstados()
        {
            var response = await _client.Get(_urlUf);

            return TratarResponseIbge(response);
        }

        private IEnumerable<Estado> TratarResponseIbge(HttpResponseMessage response)
        {
            var result = new List<Estado>();
            switch (response.StatusCode)
            {
                case HttpStatusCode.OK:
                    var dataOK = response.Content.ReadAsStringAsync().Result;
                    result = JsonConvert.DeserializeObject<List<Estado>>(dataOK);
                    break;
                default:
                    break;
            }

            return result;
        }
    }
}
