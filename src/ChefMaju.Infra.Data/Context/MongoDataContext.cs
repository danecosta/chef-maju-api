﻿using ChefMaju.Domain.Shared.Context;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace ChefMaju.Infra.Data.Context
{
    public class MongoDataContext : IMongoDataContext
    {
        private IMongoDatabase _database;

        public MongoDataContext(IOptions<DataContextSettings> settings)
        {
            var connectionString = settings.Value.ConnectionStringMongo;
            _database = (new MongoClient(connectionString)).GetDatabase(MongoUrl.Create(connectionString).DatabaseName);
        }

        public void Inserir<T>(T registro)
        {
            _database.GetCollection<T>(typeof(T).Name)
                .InsertOne(registro);
        }

        public void Inserir<T>(IEnumerable<T> registros)
        {
            _database.GetCollection<T>(typeof(T).Name)
                .InsertMany(registros);
        }

        public void Remover<T>(Expression<Func<T, bool>> filtro)
        {
            _database.GetCollection<T>(typeof(T).Name).DeleteMany(filtro);
        }

        public IEnumerable<T> Buscar<T>(Expression<Func<T, bool>> filtro = null)
        {
            return _database.GetCollection<T>(typeof(T).Name).Find(filtro == null ? (r => true) : filtro).ToList();
        }

        public IEnumerable<T> Buscar<T>(string filter)
        {
            return _database.GetCollection<T>(typeof(T).Name).Find(filter).ToList();
        }
    }
}