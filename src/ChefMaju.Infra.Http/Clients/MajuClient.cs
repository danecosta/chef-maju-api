﻿using System.Net.Http;
using System.Threading.Tasks;

namespace ChefMaju.Infra.Http.Clients
{
    public class MajuClient
    {
        private HttpClient Client { get; }

        public MajuClient(HttpClient client)
        {
            Client = client;
        }

        public async Task<HttpResponseMessage> Get(string url)
        {
            var response = await Client.GetAsync(url);

            return response;
        }
    }
}
