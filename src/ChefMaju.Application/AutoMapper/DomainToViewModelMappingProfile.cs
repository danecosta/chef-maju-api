﻿using AutoMapper;
using ChefMaju.Application.ViewModels;
using ChefMaju.Domain.EstadoAgg;
using ChefMaju.Domain.ReceitaAgg;

namespace ChefMaju.Application.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<Estado, EstadoViewModel>();
            CreateMap<Receita, ReceitaViewModel>();
        }
    }
}
