﻿using AutoMapper;
using ChefMaju.Application.Interfaces;
using ChefMaju.Application.ViewModels;
using ChefMaju.Domain.EstadoAgg.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChefMaju.Application.Services
{
    public class EstadoAppService : IEstadoAppService
    {
        private IEstadoRepository _estadoRepository;
        private readonly IMapper _mapper;

        public EstadoAppService(IEstadoRepository estadoRepository, IMapper mapper)
        {
            _estadoRepository = estadoRepository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<EstadoViewModel>> ObterEstados()
        {
            var estados = await _estadoRepository.ObterEstados();

            return _mapper.Map<IEnumerable<EstadoViewModel>>(estados.OrderBy(x => x.Nome));
        }
    }
}
