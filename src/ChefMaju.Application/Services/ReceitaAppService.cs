﻿using AutoMapper;
using ChefMaju.Application.Interfaces;
using ChefMaju.Application.ViewModels;
using ChefMaju.Domain.ClimaAgg.Repository;
using ChefMaju.Domain.ReceitaAgg.Repository;
using ChefMaju.Domain.Shared.Controle;
using ChefMaju.Domain.Shared.Enums;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChefMaju.Application.Services
{
    public class ReceitaAppService : IReceitaAppService
    {
        private IReceitaRepository _receitaRepository;
        private IControleConsultaRepository _controleRepository;
        private IClimaRepository _climaRepository;
        private IMapper _mapper;

        public ReceitaAppService(IReceitaRepository receitaRepository,
            IClimaRepository climaRepository,
            IControleConsultaRepository controleRepository,
            IMapper mapper)
        {
            _receitaRepository = receitaRepository;
            _climaRepository = climaRepository;
            _controleRepository = controleRepository;
            _mapper = mapper;
        }

        public async Task<ConsultaReceitaCidadeViewModel> ObterReceitasPorCidade(string cidade)
        {
            if (string.IsNullOrEmpty(cidade))
            {
                return new ConsultaReceitaCidadeViewModel();
            }

            var possuiLimiteOpen = _controleRepository.VerificarLimiteConsulta(ApiEnum.OpenWeather);
            var buscaClima = possuiLimiteOpen ? await _climaRepository.ObterClimaPorCidadeOpenWeather(cidade) : _climaRepository.ObterClimaPorCidade(cidade);
            if (buscaClima != null)
            {
                var filtro = DefinirFiltroReceitaPorTemperatura(buscaClima.Temperatura.TemperaturaAtual);

                var possuiLimiteEdamam = _controleRepository.VerificarLimiteConsulta(ApiEnum.Edamam);
                var buscaReceita = possuiLimiteEdamam ? await _receitaRepository.ObterReceitasEdamam(filtro) : _receitaRepository.ObterReceitas(filtro);
                if (buscaReceita != null)
                {
                    var receitas = _mapper.Map<IEnumerable<ReceitaViewModel>>(buscaReceita);

                    return new ConsultaReceitaCidadeViewModel()
                    {
                        Clima = buscaClima.Tempo.FirstOrDefault().Descricao,
                        Temperatura = buscaClima.Temperatura.TemperaturaAtual,
                        DataConsultaClima = buscaClima.DataCadastro,
                        ReceitaIndicada = receitas.FirstOrDefault(),
                        Receitas = receitas.Skip(1)
                    };
                }
            }

            return new ConsultaReceitaCidadeViewModel();
        }

        private string DefinirFiltroReceitaPorTemperatura(double temp)
        {
            if (temp >= 30)
            {
                return FiltroReceitaEnum.MaiorIgual30;
            }
            else if (temp >= 20)
            {
                return FiltroReceitaEnum.MaiorIgual20;
            }
            else
            {
                return FiltroReceitaEnum.Menor20;
            }
        }
    }
}
