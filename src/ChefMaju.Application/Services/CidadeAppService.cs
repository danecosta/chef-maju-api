﻿using ChefMaju.Application.Interfaces;
using ChefMaju.Domain.CidadeAgg.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChefMaju.Application.Services
{
    public class CidadeAppService : ICidadeAppService
    {
        private ICidadeRepository _cidadeRepository;

        public CidadeAppService(ICidadeRepository cidadeRepository)
        {
            _cidadeRepository = cidadeRepository;
        }

        public async Task<IEnumerable<string>> ObterNomeCidadesPorUf(string uf)
        {
            if (string.IsNullOrEmpty(uf))
            {
                return new List<string>();
            }

            var cidades = await _cidadeRepository.ObterCidadesPorUf(uf);

            return cidades.OrderBy(x => x.Nome).Select(x => x.Nome);
        }
    }
}
