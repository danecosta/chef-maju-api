﻿using ChefMaju.Application.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChefMaju.Application.Interfaces
{
    public interface IEstadoAppService
    {
        Task<IEnumerable<EstadoViewModel>> ObterEstados();
    }
}
