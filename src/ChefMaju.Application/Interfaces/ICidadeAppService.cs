﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChefMaju.Application.Interfaces
{
    public interface ICidadeAppService
    {
        Task<IEnumerable<string>> ObterNomeCidadesPorUf(string uf);
    }
}
