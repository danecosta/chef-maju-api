﻿using ChefMaju.Application.ViewModels;
using System.Threading.Tasks;

namespace ChefMaju.Application.Interfaces
{
    public interface IReceitaAppService
    {
        Task<ConsultaReceitaCidadeViewModel> ObterReceitasPorCidade(string cidade);
    }
}
