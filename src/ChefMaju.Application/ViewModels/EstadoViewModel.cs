﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChefMaju.Application.ViewModels
{
    public class EstadoViewModel
    {
        public string Sigla { get; set; }
        public string Nome { get; set; }
    }
}
