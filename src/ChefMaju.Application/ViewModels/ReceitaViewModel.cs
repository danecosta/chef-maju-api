﻿using System;
using System.Collections.Generic;

namespace ChefMaju.Application.ViewModels
{
    public class ReceitaViewModel
    {
        public string LinkExterno { get; set; }

        public string Nome { get; set; }

        public string LinkImagem { get; set; }

        public IEnumerable<string> Ingredientes { get; set; }

        public string TempoPreparo { get; set; }
    }
}
