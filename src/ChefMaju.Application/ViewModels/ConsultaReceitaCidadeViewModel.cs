﻿using System;
using System.Collections.Generic;

namespace ChefMaju.Application.ViewModels
{
    public class ConsultaReceitaCidadeViewModel
    {
        public ConsultaReceitaCidadeViewModel()
        {
            Receitas = new List<ReceitaViewModel>();
        }

        public string Clima { get; set; }
        public double Temperatura { get; set; }
        public DateTime DataConsultaClima { get; set; }
        public ReceitaViewModel ReceitaIndicada { get; set; }
        public IEnumerable<ReceitaViewModel> Receitas { get; set; }
    }
}
