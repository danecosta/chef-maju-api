﻿using ChefMaju.Domain.ReceitaAgg;
using System.Collections.Generic;

namespace ChefMaju.Tests.Application.Builders.Models
{
    public class ReceitasTestBuilder : BaseBuilder<ReceitasTestBuilder, IEnumerable<Receita>>
    {
        public ReceitasTestBuilder()
        {
            Model = new List<Receita>();
        }

        public ReceitasTestBuilder Default()
        {
            Model = new List<Receita>()
            {
                new Receita()
                {
                    Nome = "Vaca atolada"
                },
                new Receita()
                {
                    Nome = "Canjiquinha"
                }
            };
            return this;
        }
    }
}
