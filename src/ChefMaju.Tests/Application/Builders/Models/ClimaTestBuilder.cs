﻿using ChefMaju.Domain.ClimaAgg;
using ChefMaju.Domain.ClimaAgg.Entities;
using System.Collections.Generic;

namespace ChefMaju.Tests.Application.Builders.Models
{
    public class ClimaTestBuilder : BaseBuilder<ClimaTestBuilder, Clima>
    {
        public ClimaTestBuilder()
        {
            Model = new Clima();
        }

        public ClimaTestBuilder Default()
        {
            Model.Temperatura = new Temperatura()
            {
                TemperaturaAtual = 18
            };

            Model.Tempo = new List<Tempo>()
            {
                new Tempo()
                {
                    Nome = "Nublado",
                    Descricao = "Céu com muitas nuvens"
                }
            };

            return this;
        }
    }
}
