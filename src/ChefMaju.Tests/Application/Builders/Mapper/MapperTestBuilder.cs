﻿using AutoMapper;
using ChefMaju.Application.AutoMapper;

namespace ChefMaju.Tests.Application.Builders.Mapper
{
    public class MapperTestBuilder : BaseBuilder<MapperTestBuilder, IMapper>
    {
        public MapperTestBuilder()
        {
            var configuration = new MapperConfiguration(x =>
            {
                x.AddProfile(new DomainToViewModelMappingProfile());
                x.AddProfile(new ViewModelToDomainMappingProfile());
                x.AllowNullDestinationValues = true;
                x.AllowNullCollections = true;
            });

            Model = configuration.CreateMapper();
        }
    }
}
