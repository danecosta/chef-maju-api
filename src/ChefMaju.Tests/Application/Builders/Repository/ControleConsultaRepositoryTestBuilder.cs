﻿using ChefMaju.Domain.Shared.Controle;
using Moq;

namespace ChefMaju.Tests.Application.Builders.Repository
{
    public class ControleConsultaRepositoryTestBuilder : BaseBuilder<ControleConsultaRepositoryTestBuilder, Mock<IControleConsultaRepository>>
    {
        public ControleConsultaRepositoryTestBuilder()
        {
            Model = new Mock<IControleConsultaRepository>();
        }
    }
}
