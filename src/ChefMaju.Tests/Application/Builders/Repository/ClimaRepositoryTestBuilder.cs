﻿using ChefMaju.Domain.ClimaAgg.Repository;
using Moq;

namespace ChefMaju.Tests.Application.Builders.Repository
{
    public class ClimaRepositoryTestBuilder : BaseBuilder<ClimaRepositoryTestBuilder, Mock<IClimaRepository>>
    {
        public ClimaRepositoryTestBuilder()
        {
            Model = new Mock<IClimaRepository>();
        }
    }
}
