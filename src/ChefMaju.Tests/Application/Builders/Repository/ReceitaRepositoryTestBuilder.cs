﻿using ChefMaju.Domain.ReceitaAgg.Repository;
using Moq;

namespace ChefMaju.Tests.Application.Builders.Repository
{
    public class ReceitaRepositoryTestBuilder : BaseBuilder<ReceitaRepositoryTestBuilder, Mock<IReceitaRepository>>
    {
        public ReceitaRepositoryTestBuilder()
        {
            Model = new Mock<IReceitaRepository>();
        }
    }
}
