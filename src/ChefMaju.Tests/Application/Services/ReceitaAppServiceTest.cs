﻿using AutoMapper;
using ChefMaju.Application.Services;
using ChefMaju.Application.ViewModels;
using ChefMaju.Domain.ClimaAgg.Repository;
using ChefMaju.Domain.ReceitaAgg.Repository;
using ChefMaju.Domain.Shared.Controle;
using ChefMaju.Tests.Application.Builders.Mapper;
using ChefMaju.Tests.Application.Builders.Models;
using ChefMaju.Tests.Application.Builders.Repository;
using FluentAssertions;
using Moq;
using System.Threading.Tasks;
using Xunit;

namespace ChefMaju.Tests.Application.Services
{
    public class ReceitaAppServiceTest
    {
        IReceitaRepository _receitaRepository = new ReceitaRepositoryTestBuilder().Build().Object;
        IClimaRepository _climaRepository = new ClimaRepositoryTestBuilder().Build().Object;
        IControleConsultaRepository _controleRepository = new ControleConsultaRepositoryTestBuilder().Build().Object;
        IMapper _mapper = new MapperTestBuilder().Build();

        private ReceitaAppService MontarConstrutor()
        {
            return new ReceitaAppService(
                _receitaRepository,
                _climaRepository,
                _controleRepository,
                _mapper);
        }

        [Fact(DisplayName = "Obter receitas por cidade com retorno válido")]
        public void ObterReceitasPorCidadeValido()
        {
            #region Given
            var cidade = "Juiz de Fora";

            // Limite para consultas
            var _controleRepositoryMock = new ControleConsultaRepositoryTestBuilder().Build();
            _controleRepositoryMock.Setup(e => e.VerificarLimiteConsulta(It.IsAny<string>())).Returns(true);
            _controleRepository = _controleRepositoryMock.Object;

            // Retorno Clima
            var _climaRepositoryMock = new ClimaRepositoryTestBuilder().Build();
            _climaRepositoryMock.Setup(e => e.ObterClimaPorCidadeOpenWeather(It.IsAny<string>())).ReturnsAsync(new ClimaTestBuilder().Default().Build());
            _climaRepository = _climaRepositoryMock.Object;

            // Retorno Receita
            var _receitaRepositoryMock = new ReceitaRepositoryTestBuilder().Build();
            _receitaRepositoryMock.Setup(e => e.ObterReceitasEdamam(It.IsAny<string>())).ReturnsAsync(new ReceitasTestBuilder().Default().Build());
            _receitaRepository = _receitaRepositoryMock.Object;

            ReceitaAppService service = MontarConstrutor();
            #endregion

            #region When
            var result = service.ObterReceitasPorCidade(cidade);
            #endregion

            #region Then
            result.Should().BeOfType<Task<ConsultaReceitaCidadeViewModel>>();
            result.Result.Temperatura.Should().Be(18);
            result.Result.ReceitaIndicada.Nome.Should().Be("Vaca atolada");
            #endregion
        }

        [Fact(DisplayName = "Obter receitas por cidade com retorno vazio")]
        public void ObterReceitasPorCidadeVazio()
        {
            #region Given
            var cidade = string.Empty;

            ReceitaAppService service = MontarConstrutor();
            #endregion

            #region When
            var result = service.ObterReceitasPorCidade(cidade);
            #endregion

            #region Then
            result.Should().BeOfType<Task<ConsultaReceitaCidadeViewModel>>();
            result.Result.Clima.Should().BeNullOrEmpty();
            #endregion
        }
    }
}
