FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
WORKDIR /src
COPY ["src/ChefeMaju.API/ChefMaju.API.csproj", "src/ChefeMaju.API/"]
COPY ["src/ChefMaju.Infra.CrossCutting.Ioc/ChefMaju.Infra.Ioc.csproj", "src/ChefMaju.Infra.CrossCutting.Ioc/"]

RUN dotnet restore "src/ChefeMaju.API/ChefMaju.API.csproj"

COPY . .
WORKDIR "src/ChefeMaju.API"
RUN dotnet build "ChefMaju.API.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "ChefMaju.API.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "ChefMaju.API.dll"]